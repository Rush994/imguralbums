package com.rushapps.imguralbums.presentation.feature.albumslist

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.databinding.FragmentAlbumListBinding
import com.rushapps.imguralbums.presentation.base.BaseFragment
import com.rushapps.imguralbums.presentation.base.Constants
import com.rushapps.imguralbums.presentation.base.navigate
import com.rushapps.imguralbums.presentation.feature.albumslist.adapter.PostsDiffUtil
import com.rushapps.imguralbums.presentation.feature.albumslist.adapter.PostsRecyclerAdapter
import javax.inject.Inject

class AlbumsListFragment : BaseFragment() {
    lateinit var bindableView: FragmentAlbumListBinding
    //region onCreateView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        bindableView =
            DataBindingUtil.inflate(inflater, R.layout.fragment_album_list, container, false)
        return bindableView.root
    }
    //endregion

    val logger: (String, String) -> Unit = { str1: String, str2: String -> Log.d(str1, str2) }

    @Inject
    lateinit var viewModel: AlbumsListViewModel
    @Inject
    lateinit var postsDiffUtil: PostsDiffUtil
    @Inject
    lateinit var postsDiffResult: DiffUtil.DiffResult
    @Inject
    lateinit var postsAdapter: PostsRecyclerAdapter

    lateinit var gridLayoutManager: StaggeredGridLayoutManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gridLayoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
        initViews()
        initDisplayableData()
    }

    override fun initViews() {
        with(bindableView){
            srlTopPosts.setOnRefreshListener {
                viewModel!!.proceedAction(Action.Refresh)
            }

            rvPosts.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
            rvPosts.adapter = postsAdapter
            rvPosts.setOnScrollChangeListener { _, _, _, _, _ ->
                var item = IntArray(2)
                item = gridLayoutManager.findLastVisibleItemPositions(item)
                showUpwardFab(item[0])
                showNextListPortion(item[0])
            }

            viewModel = this@AlbumsListFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
        }
    }

    override fun initDisplayableData() {
        viewModel.postListLiveData.observe(
            viewLifecycleOwner,
            Observer { list ->
                if (postsAdapter.posts.isEmpty()) {
                    postsAdapter.posts = list
                    postsAdapter.notifyDataSetChanged()
                } else {
                    postsDiffUtil.oldList = postsAdapter.posts
                    postsDiffUtil.newList = list
                    postsDiffResult = DiffUtil.calculateDiff(postsDiffUtil)
                    postsAdapter.posts = list
                    postsDiffResult.dispatchUpdatesTo(postsAdapter)
                }
                bindableView.srlTopPosts.isRefreshing = false
            }
        )

        viewModel.proceedAction(Action.Init)
    }

    private fun showUpwardFab(lastVisibleItem: Int) {
        if (lastVisibleItem > Constants.THRESHOLD_RECYCLER_VISIBLE_ITEMS)
            viewModel.isVisibleUpwardFab = ObservableField(true)
        else viewModel.isVisibleUpwardFab = ObservableField(false)
    }

    private fun showNextListPortion(lastVisibleItem: Int) {
        val totalItemCount = gridLayoutManager.itemCount
        if (!viewModel.loading && totalItemCount <= (lastVisibleItem + Constants.THRESHOLD_RECYCLER_LOADER)) {
            viewModel.loading = true
            logger("Append to list", Constants.LOADING_POSTS)
            viewModel.proceedAction(Action.LoadNewPortion)
        }
    }
}