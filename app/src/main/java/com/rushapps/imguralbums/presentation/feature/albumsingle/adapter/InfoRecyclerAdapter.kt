package com.rushapps.imguralbums.presentation.feature.albumsingle.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.databinding.*
import com.rushapps.imguralbums.presentation.base.CellTypes
import com.rushapps.imguralbums.presentation.feature.albumsingle.AlbumSingleViewModel
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder.CommentItemHolder
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder.InfoItemHolder
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder.TagRecyclerItemHolder
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder.TitleItemHolder
import com.rushapps.imguralbums.presentation.viewmodel.CommentViewModel
import com.rushapps.imguralbums.presentation.viewmodel.PostViewModel
import com.rushapps.imguralbums.presentation.viewmodel.TagViewModel

class InfoRecyclerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var post: PostViewModel = PostViewModel()
    private var comments: List<CommentViewModel> = mutableListOf()
    private var tags: List<TagViewModel> = mutableListOf()
    private val viewPool = RecyclerView.RecycledViewPool()

    lateinit var viewModel: AlbumSingleViewModel

    fun setPost(post: PostViewModel) {
        this.post = post
    }

    fun setTagList(tags: List<TagViewModel>) {
        this.tags = tags
    }

    fun setCommentList(comments: List<CommentViewModel>) {
        this.comments = comments
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int =
        when (position) {
            0 -> CellTypes.PostTitle.value
            1 -> CellTypes.PostTag.value
            2 -> CellTypes.PostInfo.value
            else -> CellTypes.PostComment.value
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) =
        when (viewType) {
            CellTypes.PostComment.value -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding: PostSingleCommentItemBinding =
                    DataBindingUtil.inflate(
                        inflater,
                        R.layout.post_single_comment_item,
                        parent,
                        false
                    )
                CommentItemHolder(binding)
            }
            CellTypes.PostTitle.value -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding: PostSingleTitleItemBinding =
                    DataBindingUtil.inflate(
                        inflater,
                        R.layout.post_single_title_item,
                        parent,
                        false
                    )
                TitleItemHolder(binding)
            }
            CellTypes.PostInfo.value -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding: PostSingleInfoItemBinding =
                    DataBindingUtil.inflate(inflater, R.layout.post_single_info_item, parent, false)
                InfoItemHolder(binding)
            }
            CellTypes.PostTag.value -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding: PostSingleTagrecyclerItemBinding =
                    DataBindingUtil.inflate(
                        inflater,
                        R.layout.post_single_tagrecycler_item,
                        parent,
                        false
                    )
                TagRecyclerItemHolder(binding)
            }
            else -> TODO()
        }

    override fun getItemCount(): Int = comments.size + 3

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        when (getItemViewType(position)) {
            CellTypes.PostTitle.value -> {
                holder as TitleItemHolder
                holder.bind(post.title)
            }
            CellTypes.PostTag.value -> {
                holder as TagRecyclerItemHolder
                holder.bind(tags, viewPool)
            }
            CellTypes.PostInfo.value -> {
                holder as InfoItemHolder
                holder.bind(post, viewModel)
            }
            CellTypes.PostComment.value -> {
                holder as CommentItemHolder
                val item = comments[position - 3]
                holder.bind(item)
            }
        }
    }
}