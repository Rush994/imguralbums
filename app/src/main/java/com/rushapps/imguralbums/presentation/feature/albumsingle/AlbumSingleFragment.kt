package com.rushapps.imguralbums.presentation.feature.albumsingle

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.databinding.FragmentAlbumSingleBinding
import com.rushapps.imguralbums.presentation.base.BaseFragment
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.InfoRecyclerAdapter
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.MediasRecyclerAdapter
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder.VideoItemHolder
import javax.inject.Inject

class AlbumSingleFragment: BaseFragment(), DetachViewListener {
    lateinit var bindableView: FragmentAlbumSingleBinding
    //region onCreateView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        bindableView =
            DataBindingUtil.inflate(inflater, R.layout.fragment_album_single, container, false)
        return bindableView.root
    }
    //endregion

    @Inject
    lateinit var viewModel: AlbumSingleViewModel
    @Inject
    lateinit var layoutManagerInfo: LinearLayoutManager
    @Inject
    lateinit var layoutManagerMedias: LinearLayoutManager
    @Inject
    lateinit var infoAdapter: InfoRecyclerAdapter
    @Inject
    lateinit var mediasAdapter: MediasRecyclerAdapter

    private val args: AlbumSingleFragmentArgs by navArgs()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        viewModel.load(args.argPost)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        infoAdapter.viewModel = viewModel
        initViews()
        initDisplayableData()
    }

    override fun initViews() {
        with(bindableView)
        {
            rvPostMedias.layoutManager = layoutManagerMedias
            rvPostMedias.adapter = mediasAdapter
            rvPostInfos.adapter = infoAdapter
            rvPostInfos.layoutManager = layoutManagerInfo
        }
    }

    override fun initDisplayableData() {
        viewModel.observedImagesLiveData.observe(
            viewLifecycleOwner,
            Observer { list ->
                mediasAdapter.setMediaList(list)
            }
        )

        viewModel.observedJointPostLiveData.observe(
            viewLifecycleOwner,
            Observer { item ->
                infoAdapter.setPost(viewModel.post)
                infoAdapter.setTagList(item.tags)
                infoAdapter.setCommentList(item.comments)
            }
        )

        viewModel.startActivityLiveData.observe(
            viewLifecycleOwner,
            Observer { intent->
                intent?.let { startActivity(it) }
            }
        )

        viewModel.displayData()
    }

    override fun onResume() {
        super.onResume()
        resumeLastVideo()
    }
    override fun resumeLastVideo() {
        val holder = bindableView.rvPostMedias.findViewHolderForLayoutPosition(
            layoutManagerMedias.findLastVisibleItemPosition()
        )
        if (holder is VideoItemHolder)
            if (holder.pausedOnCollapse) {
                holder.resumeVideo()
                holder.pausedOnCollapse = false
            }
    }

    override fun onPause() {
        super.onPause()
        pauseLastVideo()
    }
    override fun pauseLastVideo() {
        val holder = bindableView.rvPostMedias.findViewHolderForLayoutPosition(
            layoutManagerMedias.findLastVisibleItemPosition()
        )
        if (holder is VideoItemHolder)
            holder.pauseVideo(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
    }

    override fun releasePlayer() {
        for (i in mediasAdapter.medias.indices) {
            val holder = bindableView.rvPostMedias.findViewHolderForLayoutPosition(i)
            if (holder is VideoItemHolder)
                holder.releasePlayer()
        }
    }

}