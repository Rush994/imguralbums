package com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.databinding.PostListItemBinding
import com.rushapps.imguralbums.databinding.PostSingleCommentItemBinding
import com.rushapps.imguralbums.presentation.viewmodel.CommentViewModel

class CommentItemHolder(
    private val binding: PostSingleCommentItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    private val materialGenerator: ColorGenerator = ColorGenerator.MATERIAL
    lateinit var roundDrawable: TextDrawable
    fun bind(item: CommentViewModel) {
        when (item.platform) {
            "iphone" -> item.plImageId = R.drawable.ic_apple
            "android" -> item.plImageId = R.drawable.ic_android
            "desktop" -> item.plImageId = R.drawable.ic_desktop
            "api" -> item.plImageId = R.drawable.ic_api
        }
        roundDrawable = TextDrawable.builder()
            .buildRound(item.author[0].toString(), materialGenerator.randomColor)
        binding.ivCommentAuthorImage.setImageDrawable(roundDrawable)
        binding.item = item
        binding.executePendingBindings()
    }
}