package com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.rushapps.imguralbums.databinding.PostSingleInfoItemBinding
import com.rushapps.imguralbums.presentation.feature.albumsingle.AlbumSingleViewModel
import com.rushapps.imguralbums.presentation.viewmodel.PostViewModel

class InfoItemHolder(private val binding: PostSingleInfoItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(item: PostViewModel, viewModel: AlbumSingleViewModel) {
        binding.viewModel = viewModel
        binding.item = item
        binding.executePendingBindings()
    }
}