package com.rushapps.imguralbums.presentation.viewmodel

data class JointPostViewModel(
    var comments: List<CommentViewModel> = listOf(),
    var tags: List<TagViewModel> = listOf(),

    var post: PostViewModel = PostViewModel()
)