package com.rushapps.imguralbums.presentation.feature.albumsingle

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.domain.entity.PostEntity
import com.rushapps.imguralbums.domain.entity.toViewModel
import com.rushapps.imguralbums.domain.interator.Loaders
import com.rushapps.imguralbums.domain.interator.Observers
import com.rushapps.imguralbums.domain.usecase.load.LoadImagesUseCase
import com.rushapps.imguralbums.presentation.base.BaseViewModel
import com.rushapps.imguralbums.presentation.base.SingleLiveEvent
import com.rushapps.imguralbums.presentation.base.StateObject
import com.rushapps.imguralbums.presentation.viewmodel.ImageViewModel
import com.rushapps.imguralbums.presentation.viewmodel.JointPostViewModel
import com.rushapps.imguralbums.presentation.viewmodel.PostViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject

class AlbumSingleViewModel @Inject constructor(
    private val observers: Observers,
    private val loaders: Loaders,
    private val context: Context
) : BaseViewModel() {

    val observedImagesLiveData = MutableLiveData<List<ImageViewModel>>()
    val observedJointPostLiveData = MutableLiveData<JointPostViewModel>()
    val startActivityLiveData = SingleLiveEvent<Intent>()

    lateinit var post: PostViewModel
    lateinit var mediaUri: String

    fun load(postArg: PostViewModel) {
        post = postArg
        mediaUri = "https://imgur.com/gallery/${post.id}"
    }

    fun displayData(){
        observePost()
        loadPost()
    }

    private fun observePost() {
        observers.images(post.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.toViewModel() }
            .subscribe({
                if (it.isNotEmpty())
                    observedImagesLiveData.value = it
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                }).also { disposables.add(it) }

        observers.jointPost(post.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.toViewModel() }
            .subscribe({
                observedJointPostLiveData.value = it
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                }).also { disposables.add(it) }
    }

    private fun loadPost(){
        when (post.isAlbum) {
            true -> imagesListLoading()
            false -> coverImageInsertion()
        }

        loaders.comments(post.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                        Log.d("Not get(http): ", it.toString())
                    } else {
                        Log.d("Not get(java): ", it.toString())
                    }
                })
            .also { disposables.add(it) }
    }

    private fun imagesListLoading() =
        loaders.images(post.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                        Log.d("Not get(http): ", it.toString())
                    } else {
                        Log.d("Not get(java): ", it.toString())
                    }
                })
            .also { disposables.add(it) }

    private fun coverImageInsertion() =
        loaders.imageInsert(
            PostEntity(
                id = post.id,
                mediaId = post.mediaId,
                title = post.title,
                datetime = post.datetime,
                cover = post.cover,
                owner = post.owner,
                views = post.views,
                link = post.link,
                thumbUps = post.thumbUps,
                thumbDowns = post.thumbDowns,
                commentCount = post.commentCount,
                isAlbum = post.isAlbum,
                mediaCount = post.mediaCount
            )
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                        Log.d("Not get(http): ", it.toString())
                    } else {
                        Log.d("Not get(java): ", it.toString())
                    }
                })
            .also { disposables.add(it) }

    fun sharePost() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(
            Intent.EXTRA_SUBJECT,
            "Shared by \"${context.resources.getString(R.string.app_name)}\": ${post.title}"
        )
        intent.putExtra(Intent.EXTRA_TEXT, "https://imgur.com/gallery/${post.id}")
        startActivityLiveData.postValue(Intent.createChooser(intent, "Share post URL"))
    }
}