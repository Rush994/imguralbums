package com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rushapps.imguralbums.databinding.PostSingleTagrecyclerItemBinding
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.TagsRecyclerAdapter
import com.rushapps.imguralbums.presentation.viewmodel.TagViewModel

class TagRecyclerItemHolder(private val binding: PostSingleTagrecyclerItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: List<TagViewModel>, viewPool: RecyclerView.RecycledViewPool) {
        with(binding.rvPostTags)
        {
            layoutManager =
                LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = TagsRecyclerAdapter(item)
            setRecycledViewPool(viewPool)
        }
        binding.executePendingBindings()
    }
}