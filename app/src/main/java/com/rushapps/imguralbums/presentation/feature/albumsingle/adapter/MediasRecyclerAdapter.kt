package com.rushapps.imguralbums.presentation.feature.albumsingle.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.databinding.MediaExoplayerItemBinding
import com.rushapps.imguralbums.databinding.MediaImageItemBinding
import com.rushapps.imguralbums.databinding.PostTagItemBinding
import com.rushapps.imguralbums.presentation.base.MediaTypes
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder.ImageItemHolder
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder.VideoItemHolder
import com.rushapps.imguralbums.presentation.viewmodel.ImageViewModel

class MediasRecyclerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    var medias: List<ImageViewModel> = mutableListOf()

    fun setMediaList(media: List<ImageViewModel>) {
        this.medias = media
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        var type = MediaTypes.Image.value
        val item = medias.get(position)
        if (item.mp4 == null)
            type = MediaTypes.Image.value
        if(item.link.endsWith(".gif"))
            type = MediaTypes.Video.value
        if(item.link.endsWith(".mp4"))
            type = MediaTypes.Video.value
        return type
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when(viewType){
            MediaTypes.Image.value ->  {
                val inflater = LayoutInflater.from(parent.context)
                val binding: MediaImageItemBinding =
                    DataBindingUtil.inflate(inflater, R.layout.media_image_item, parent, false)
                ImageItemHolder(binding)
            }
            MediaTypes.Video.value -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding: MediaExoplayerItemBinding =
                    DataBindingUtil.inflate(inflater, R.layout.media_exoplayer_item, parent, false)
                VideoItemHolder(binding)
            }
            else -> TODO()
        }

    override fun getItemCount(): Int = medias.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = medias.get(position)
        when(getItemViewType(position)){
            MediaTypes.Image.value ->{
                holder as ImageItemHolder
                holder.bind(item)
            }
            MediaTypes.Video.value ->{
                holder as VideoItemHolder
                holder.bind(item)
            }
        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        when(getItemViewType(holder.layoutPosition)) {
            MediaTypes.Video.value -> {
                holder as VideoItemHolder
                holder.releasePlayer()
            }
        }
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        when(getItemViewType(holder.layoutPosition)) {
            MediaTypes.Video.value -> {
                holder as VideoItemHolder
                holder.initPlayer()
            }
        }
    }
}