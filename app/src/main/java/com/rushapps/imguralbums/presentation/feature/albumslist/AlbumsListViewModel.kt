package com.rushapps.imguralbums.presentation.feature.albumslist

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.rushapps.imguralbums.data.network.api.responseapi.ErrorResp
import com.rushapps.imguralbums.domain.usecase.DeletePostsUseCase
import com.rushapps.imguralbums.domain.usecase.load.LoadPostListUseCase
import com.rushapps.imguralbums.domain.usecase.observe.ObservePostListUseCase
import com.rushapps.imguralbums.presentation.base.BaseViewModel
import com.rushapps.imguralbums.presentation.base.navigate
import com.rushapps.imguralbums.presentation.viewmodel.PostViewModel
import com.rushapps.imguralbums.presentation.viewmodel.toViewModel
import com.squareup.moshi.Moshi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject

class AlbumsListViewModel @Inject constructor(
    private val observePostsUseCase: ObservePostListUseCase,
    private val loadPostListUseCase: LoadPostListUseCase,
    private val deletePostsUseCase: DeletePostsUseCase,
    private val fragment: AlbumsListFragment
) : BaseViewModel() {
    var name = ObservableField("Greg")
    var isVisiblePostList = ObservableField(false)
    var isVisibleUpwardFab = ObservableField(false)

    val postListLiveData = MutableLiveData<List<PostViewModel>>()

    private var loadPaginator = PublishProcessor.create<Int>()
    var lastLoadedPage = 0
    var loading = false

    fun proceedAction(action: Action)
    {
        when(action){
            is Action.Init -> initList()
            is Action.Refresh -> refreshList()
            is Action.LoadNewPortion -> loadList()
        }
    }

    private fun initList() {
        lastLoadedPage = 0
        initObserver()
        initLoader()
        android.os.Handler().postDelayed({
            loadList()
        }, 100)
    }

    private fun refreshList() =
        deletePostsUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                lastLoadedPage = 0
                loadList()
            }.also { disposables.add(it) }

    private fun loadList() {
        lastLoadedPage++
        loadPaginator.onNext(lastLoadedPage)
    }

    private fun initObserver() =
        observePostsUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.toViewModel() }
            .subscribe({
                if (it.isNotEmpty()) {
                    postListLiveData.value = it
                    isVisiblePostList = ObservableField(true)
                    loading = false
                }
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                })
            .also { disposables.add(it) }

    private fun initLoader() =
        loadPaginator
            .subscribeOn(Schedulers.io())
            .concatMapCompletable {
                loadPostListUseCase(page = it)
                    .subscribeOn(Schedulers.io())
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                isVisiblePostList = ObservableField(false)
            }
            .subscribe({
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                        val errorObject = Moshi.Builder().build().adapter(ErrorResp::class.java)
                            .fromJson(resp.toString())
                        Log.d("Not get: ", errorObject!!.error)
                    }
                    Log.d("Not get: ", it.toString())
                }).also { disposables.add(it) }

    fun onItemClick(item: PostViewModel) =
        fragment.navigate(AlbumsListFragmentDirections.actionToAlbumSingleFragment(item))
}