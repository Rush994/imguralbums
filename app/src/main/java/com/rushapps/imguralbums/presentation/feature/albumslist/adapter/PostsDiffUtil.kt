package com.rushapps.imguralbums.presentation.feature.albumslist.adapter

import androidx.recyclerview.widget.DiffUtil
import com.rushapps.imguralbums.presentation.viewmodel.PostViewModel
import javax.inject.Inject

class PostsDiffUtil @Inject constructor(): DiffUtil.Callback() {
    var oldList: List<PostViewModel> = listOf()
    var newList: List<PostViewModel> = listOf()

    override fun getOldListSize(): Int = oldList.size
    override fun getNewListSize(): Int = newList.size
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].id == newList[newItemPosition].id
    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition] == newList[newItemPosition]
}