package com.rushapps.imguralbums.presentation.base

data class StateObject(
    var progressVisible: Boolean = false,
    var errorText: String? = null,
    var completed: Boolean = false
)