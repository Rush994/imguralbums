package com.rushapps.imguralbums.presentation.base

import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.rushapps.imguralbums.R
import com.squareup.picasso.Picasso

@BindingAdapter("marginStart")
fun TextView.marginStart(visible: Boolean) {
    val param = layoutParams as ConstraintLayout.LayoutParams
    if (visible)
    param.setMargins(
        resources.getDimension(R.dimen.margin_max).toInt(),
        resources.getDimension(R.dimen.margin_null).toInt(),
        resources.getDimension(R.dimen.margin_null).toInt(),
        resources.getDimension(R.dimen.margin_null).toInt()
    )
    else
        param.setMargins(
            resources.getDimension(R.dimen.margin_null).toInt(),
            resources.getDimension(R.dimen.margin_null).toInt(),
            resources.getDimension(R.dimen.margin_null).toInt(),
            resources.getDimension(R.dimen.margin_null).toInt()
        )
    layoutParams = param
}

@BindingAdapter("uriSrc")
fun ImageView.uriSrc(mediaId: String){
    Glide
        .with(context)
        .load("https://i.imgur.com/${mediaId}.jpg")
        .placeholder(R.drawable.ic_default_picture)
        .into(this)
}

