package com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.rushapps.imguralbums.databinding.PostSingleTitleItemBinding

class TitleItemHolder(private val binding: PostSingleTitleItemBinding): RecyclerView.ViewHolder(binding.root) {
    fun bind(item: String) {
        binding.title = item
        binding.executePendingBindings()
    }
}