package com.rushapps.imguralbums.presentation.feature.albumslist.di

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.rushapps.imguralbums.di.scope.ViewModelKey
import com.rushapps.imguralbums.domain.usecase.DeletePostsUseCase
import com.rushapps.imguralbums.domain.usecase.load.LoadPostListUseCase
import com.rushapps.imguralbums.domain.usecase.observe.ObservePostListUseCase
import com.rushapps.imguralbums.presentation.feature.albumslist.AlbumsListFragment
import com.rushapps.imguralbums.presentation.feature.albumslist.AlbumsListViewModel
import com.rushapps.imguralbums.presentation.feature.albumslist.adapter.PostsDiffUtil
import com.rushapps.imguralbums.presentation.feature.albumslist.adapter.PostsRecyclerAdapter
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class AlbumsListModule {
    @Provides
    @IntoMap
    @ViewModelKey(AlbumsListViewModel::class)
    fun provideViewModel(observePostsUseCase: ObservePostListUseCase,
                         loadPostListUseCase: LoadPostListUseCase,
                         deletePostsUseCase: DeletePostsUseCase,
                         fragment: AlbumsListFragment
    ): ViewModel =
        AlbumsListViewModel(observePostsUseCase, loadPostListUseCase, deletePostsUseCase, fragment)

    @Provides
    fun providePostsAdapter(viewModel: AlbumsListViewModel): PostsRecyclerAdapter =
        PostsRecyclerAdapter(viewModel)

    @Provides
    fun providePostsDiffResult(postsDiffUtil: PostsDiffUtil): DiffUtil.DiffResult =
        DiffUtil.calculateDiff(postsDiffUtil)
}