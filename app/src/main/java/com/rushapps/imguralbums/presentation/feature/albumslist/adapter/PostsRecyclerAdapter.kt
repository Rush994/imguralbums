package com.rushapps.imguralbums.presentation.feature.albumslist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.databinding.PostListItemBinding
import com.rushapps.imguralbums.presentation.feature.albumslist.AlbumsListViewModel
import com.rushapps.imguralbums.presentation.viewmodel.PostViewModel


class PostsRecyclerAdapter(val viewModel: AlbumsListViewModel) :
    RecyclerView.Adapter<PostsRecyclerAdapter.SingleItemHolder>() {

    var posts: List<PostViewModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SingleItemHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: PostListItemBinding =
            DataBindingUtil.inflate(inflater, R.layout.post_list_item, parent, false)
        return SingleItemHolder(binding)
    }

    override fun getItemCount(): Int = posts.size

    override fun onBindViewHolder(holder: SingleItemHolder, position: Int) =
        holder.bind(posts[position])

    inner class SingleItemHolder(val binding: PostListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: PostViewModel) {
            item.mediaCount?.let {
                if (it > 1)
                    item.visible = true
            }
            binding.item = item
            binding.viewModel = viewModel
            binding.executePendingBindings()
        }
    }
}