package com.rushapps.imguralbums.presentation.viewmodel

data class TagViewModel(
    val name: String,
    val displayName: String,
    val followers: Int,
    val totalItems: Int
)