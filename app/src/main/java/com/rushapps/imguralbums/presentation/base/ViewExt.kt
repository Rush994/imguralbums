package com.rushapps.imguralbums.presentation.base

import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.Navigation

fun Fragment.navigate(direction: NavDirections) =
    Navigation.findNavController(view!!).navigate(direction)
