package com.rushapps.imguralbums.presentation.feature.albumsingle.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.databinding.PostSingleCommentItemBinding
import com.rushapps.imguralbums.databinding.PostTagItemBinding
import com.rushapps.imguralbums.presentation.feature.albumsingle.AlbumSingleViewModel
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder.CommentItemHolder
import com.rushapps.imguralbums.presentation.viewmodel.TagViewModel

class TagsRecyclerAdapter(private val tags: List<TagViewModel>) :
    RecyclerView.Adapter<TagsRecyclerAdapter.SingleItemHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SingleItemHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: PostTagItemBinding =
            DataBindingUtil.inflate(inflater, R.layout.post_tag_item, parent, false)
        return SingleItemHolder(binding)
    }

    override fun getItemCount(): Int = tags.size

    override fun onBindViewHolder(holder: SingleItemHolder, position: Int) {
        val item = tags[position]
        holder.bind(item)
    }

    inner class SingleItemHolder(private val binding: PostTagItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TagViewModel) {
            binding.item = item
            binding.executePendingBindings()
        }
    }
}