package com.rushapps.imguralbums.presentation.base

enum class CellTypes(val value: Int) {
    PostTitle(1),
    PostInfo(2),
    PostTag(3),
    PostComment(4)
}