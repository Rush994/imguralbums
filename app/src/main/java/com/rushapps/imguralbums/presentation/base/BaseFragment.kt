package com.rushapps.imguralbums.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment() {

    abstract fun initViews()

    abstract fun initDisplayableData()

    fun fragmentNavigate(view: View, destination: Int) =
        Navigation.findNavController(view).navigate(destination, null)

    fun directionNavigate(destination: NavDirections) =
        Navigation.createNavigateOnClickListener(destination)
}