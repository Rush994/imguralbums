package com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.databinding.MediaImageItemBinding
import com.rushapps.imguralbums.presentation.viewmodel.ImageViewModel

class ImageItemHolder(private val binding: MediaImageItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: ImageViewModel){
        binding.item = item
        binding.executePendingBindings()
    }
}