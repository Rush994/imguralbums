package com.rushapps.imguralbums.presentation.feature.albumslist

sealed class Action {
    object Init: Action()
    object Refresh: Action()
    object LoadNewPortion: Action()
}