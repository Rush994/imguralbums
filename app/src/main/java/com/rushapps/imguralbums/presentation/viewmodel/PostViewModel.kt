package com.rushapps.imguralbums.presentation.viewmodel

import android.os.Parcelable
import com.rushapps.imguralbums.domain.entity.PostEntity
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostViewModel(
    var id: String = "",
    var mediaId: String = "",
    val title: String = "",
    val datetime: Long = 0,
    val cover: String? = null,
    val owner: String = "",
    val views: Int = 0,
    val link: String = "",
    val thumbUps: Int = 0,
    val thumbDowns: Int = 0,
    val commentCount: Int = 0,
    val isAlbum: Boolean = false,
    var mediaCount: Int? = null,
    var visible: Boolean = false
): Parcelable

fun PostEntity.toViewModel(): PostViewModel =
    PostViewModel(
        id = id,
        mediaId = mediaId,
        title = title,
        datetime = datetime,
        cover = cover,
        owner = owner,
        views = views,
        link = link,
        thumbUps = thumbUps,
        thumbDowns = thumbDowns,
        commentCount = commentCount,
        isAlbum = isAlbum,
        mediaCount = mediaCount
    )

fun List<PostEntity>.toViewModel(): List<PostViewModel> =
    map { it.toViewModel() }