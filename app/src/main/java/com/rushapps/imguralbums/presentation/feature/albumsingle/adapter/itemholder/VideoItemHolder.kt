package com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.itemholder

import android.net.Uri
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.LoopingMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.databinding.MediaExoplayerItemBinding
import com.rushapps.imguralbums.presentation.viewmodel.ImageViewModel

class VideoItemHolder(private val binding: MediaExoplayerItemBinding) : RecyclerView.ViewHolder(binding.root) {
    lateinit var simpleExoplayer: SimpleExoPlayer
    var pausedOnCollapse = false
    var isVideoAdded = false

    private val bandwidthMeter by lazy {
        DefaultBandwidthMeter.Builder(binding.root.context)
    }
    private val extractorsFactory by lazy {
        DefaultExtractorsFactory()
    }
    var isMute: Boolean = true
    private var uriLink = Uri.EMPTY

    fun bind(item: ImageViewModel){
        uriLink = Uri.parse("https://i.imgur.com/${item.mediaId}.mp4")
        item.link = "https://i.imgur.com/${item.mediaId}.mp4"
        with(binding){
            fabVolume.setOnClickListener {
                if (isMute) {
                    unMuteVideo()
                    fabVolume.setImageResource(R.drawable.ic_volume_up)
                } else {
                    muteVideo()
                    fabVolume.setImageResource(R.drawable.ic_volume_off)
                }
                isMute = isMute.not()
            }
            simpleExoPlayerView.useController = false
            simpleExoPlayerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
        }
    }

    private fun initializeExoplayer() {
        val trackSelector = DefaultTrackSelector()
        simpleExoplayer = ExoPlayerFactory.newSimpleInstance(binding.root.context, trackSelector)
    }
    fun initPlayer(){
        initializeExoplayer()
        binding.simpleExoPlayerView.player = simpleExoplayer
        setPlayerUri()
        muteVideo(); isMute = true
    }

    private fun setPlayerUri() {
        // Produces DataSource instances through which medias data is loaded.
        val dataSourceFactory = DefaultDataSourceFactory(
            binding.root.context, Util.getUserAgent(
                binding.root.context,
                binding.root.context.getString(R.string.app_name)
            )
        )
        val videoSource = ExtractorMediaSource(
            uriLink,
            dataSourceFactory, extractorsFactory, null, null
        )

        val loopingSource = LoopingMediaSource(videoSource)
        isVideoAdded = true
        simpleExoplayer.prepare(loopingSource)
        resumeVideo(true)
    }
    fun muteVideo(){
        simpleExoplayer.volume = 0f
    }
    fun unMuteVideo(){
        simpleExoplayer.volume = 1f
    }
    fun resumeVideo(isScroll: Boolean = false){
        if (!pausedOnCollapse && !isScroll)
            simpleExoplayer.seekTo(0)
        simpleExoplayer.playWhenReady = true
        with(binding){
            simpleExoPlayerView.visibility = View.VISIBLE
            fabVolume.visibility = View.VISIBLE
            coverExoPlayer.visibility = View.GONE
        }
    }
    fun pauseVideo(pausedOnCollapse: Boolean = false){
        this.pausedOnCollapse = pausedOnCollapse
        simpleExoplayer.playWhenReady = false
    }

    fun releasePlayer() {
        simpleExoplayer.release()
    }
}