package com.rushapps.imguralbums.presentation.base

enum class MediaTypes(val value: Int) {
    Image(1),
    Video(2)
}