package com.rushapps.imguralbums.presentation.feature.root

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel

class MainViewModel: ViewModel() {
    var name = ObservableField("Greg")
}