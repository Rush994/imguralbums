package com.rushapps.imguralbums.presentation.feature.albumsingle

interface DetachViewListener {
    fun releasePlayer()
    fun pauseLastVideo()
    fun resumeLastVideo()
}