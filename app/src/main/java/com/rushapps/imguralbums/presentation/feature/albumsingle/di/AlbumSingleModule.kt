package com.rushapps.imguralbums.presentation.feature.albumsingle.di

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rushapps.imguralbums.di.scope.ViewModelKey
import com.rushapps.imguralbums.domain.interator.Loaders
import com.rushapps.imguralbums.domain.interator.Observers
import com.rushapps.imguralbums.domain.usecase.load.LoadPostListUseCase
import com.rushapps.imguralbums.domain.usecase.observe.ObservePostListUseCase
import com.rushapps.imguralbums.presentation.feature.albumsingle.AlbumSingleFragment
import com.rushapps.imguralbums.presentation.feature.albumsingle.AlbumSingleViewModel
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.InfoRecyclerAdapter
import com.rushapps.imguralbums.presentation.feature.albumsingle.adapter.MediasRecyclerAdapter
import com.rushapps.imguralbums.presentation.feature.albumslist.AlbumsListViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class AlbumSingleModule {
    @Provides
    @IntoMap
    @ViewModelKey(AlbumSingleViewModel::class)
    fun provideViewModel(
        observers: Observers,
        loaders: Loaders,
        fragment: AlbumSingleFragment
    ): ViewModel =
        AlbumSingleViewModel(
            observers = observers,
            loaders = loaders,
            context = fragment.context!!
        )

    @Provides
    fun provideLayoutManagerInfo(fragment: AlbumSingleFragment): LinearLayoutManager =
        LinearLayoutManager(fragment.context, RecyclerView.VERTICAL, false)

    @Provides
    fun provideMediasAdapter(): MediasRecyclerAdapter =
        MediasRecyclerAdapter()

    @Provides
    fun provideInfoAdapter(): InfoRecyclerAdapter =
        InfoRecyclerAdapter()
}