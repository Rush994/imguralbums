package com.rushapps.imguralbums.presentation.feature.root

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rushapps.imguralbums.R
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {

    val navController: NavController
        get() = findNavController(R.id.nav_host_fragment)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
