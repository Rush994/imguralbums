package com.rushapps.imguralbums.di.module

import com.rushapps.imguralbums.presentation.feature.albumsingle.AlbumSingleFragment
import com.rushapps.imguralbums.presentation.feature.albumsingle.di.AlbumSingleModule
import com.rushapps.imguralbums.presentation.feature.albumslist.AlbumsListFragment
import com.rushapps.imguralbums.presentation.feature.albumslist.di.AlbumsListModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector(modules = [AlbumsListModule::class])
    abstract fun contributeAlbumListFragment(): AlbumsListFragment

    @ContributesAndroidInjector(modules = [AlbumSingleModule::class])
    abstract fun contributeAlbumSingleFragment(): AlbumSingleFragment
}
