package com.rushapps.imguralbums.di.module

import com.rushapps.imguralbums.data.db.source.IPostsDBSource
import com.rushapps.imguralbums.data.db.source.PostsDBSource
import com.rushapps.imguralbums.data.network.source.IPostsNetSource
import com.rushapps.imguralbums.data.network.source.PostsNetSource
import com.rushapps.imguralbums.data.repository.PostsRepository
import com.rushapps.imguralbums.di.scope.PerApplication
import com.rushapps.imguralbums.domain.IPostsRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {

    @PerApplication
    @Binds
    abstract fun bindPostsNetSource(source: PostsNetSource): IPostsNetSource

    @PerApplication
    @Binds
    abstract fun bindPostsDBSource(source: PostsDBSource): IPostsDBSource

    @PerApplication
    @Binds
    abstract fun bindPostsRepository(source: PostsRepository): IPostsRepository
}