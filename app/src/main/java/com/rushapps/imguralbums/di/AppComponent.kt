package com.rushapps.imguralbums.di

import android.content.Context
import com.rushapps.imguralbums.App
import com.rushapps.imguralbums.di.module.*
import com.rushapps.imguralbums.di.scope.PerApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ViewModelFactoryModule::class,
        ActivityBuilderModule::class,
        FragmentBuildersModule::class,
        NetworkModule::class,
        RoomModule::class,
        DataModule::class]
)
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }
}