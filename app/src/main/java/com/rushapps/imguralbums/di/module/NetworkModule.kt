package com.rushapps.imguralbums.di.module

import com.rushapps.imguralbums.BuildConfig
import com.rushapps.imguralbums.data.network.RequestInterceptor
import com.rushapps.imguralbums.data.network.api.ImgurApi
import com.rushapps.imguralbums.data.network.api.ImgurApi.Companion.BASE_URL
import com.rushapps.imguralbums.di.scope.PerApplication
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
class NetworkModule {
    @PerApplication
    @Provides
    fun provideRetrofit(requestInterceptor: RequestInterceptor): Retrofit {

        val httpClient = OkHttpClient.Builder()

        httpClient.addInterceptor(HttpLoggingInterceptor().apply {
            level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        })
            .addInterceptor(requestInterceptor)

        return Retrofit.Builder()
            .client(httpClient.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
    }

    @PerApplication
    @Provides
    fun provideApi(retrofit: Retrofit): ImgurApi =
        retrofit.create(ImgurApi::class.java)
}