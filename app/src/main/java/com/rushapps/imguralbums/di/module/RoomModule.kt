package com.rushapps.imguralbums.di.module

import android.content.Context
import androidx.room.Room
import com.rushapps.imguralbums.R
import com.rushapps.imguralbums.data.db.AppDatabase
import com.rushapps.imguralbums.data.db.dao.*
import com.rushapps.imguralbums.di.scope.PerApplication
import dagger.Module
import dagger.Provides

@Module
class RoomModule {
    @PerApplication
    @Provides
    fun providesRoomDatabase(mContext: Context): AppDatabase =
        Room.databaseBuilder(mContext, AppDatabase::class.java, "${mContext.getString(R.string.app_name)}DB").build()

    @PerApplication
    @Provides
    fun providePostsDao(appDatabase: AppDatabase): PostsDao =
        appDatabase.getPostsDao()

    @PerApplication
    @Provides
    fun provideImagesDao(appDatabase: AppDatabase): ImagesDao =
        appDatabase.getImagesDao()

    @PerApplication
    @Provides
    fun provideTagsDao(appDatabase: AppDatabase): TagsDao =
        appDatabase.getTagsDao()

    @PerApplication
    @Provides
    fun provideCommentsDao(appDatabase: AppDatabase): CommentsDao =
        appDatabase.getCommentsDao()

    @PerApplication
    @Provides
    fun providePostTagJoinsDao(appDatabase: AppDatabase): PostTagJoinsDao =
        appDatabase.getPostTagJoinsDao()
}