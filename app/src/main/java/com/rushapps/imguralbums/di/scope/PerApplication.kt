package com.rushapps.imguralbums.di.scope

import javax.inject.Scope

@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApplication