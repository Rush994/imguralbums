package com.rushapps.imguralbums.domain.usecase.load

import com.rushapps.imguralbums.domain.IPostsRepository
import com.rushapps.imguralbums.domain.entity.TagEntity
import io.reactivex.Single
import javax.inject.Inject

class LoadTagListUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(): Single<List<TagEntity>> =
        postsRepository.loadTags()
}