package com.rushapps.imguralbums.domain.usecase

import com.rushapps.imguralbums.domain.IPostsRepository
import io.reactivex.Completable
import javax.inject.Inject

class DeletePostsUseCase @Inject constructor(private val postsRepository: IPostsRepository
) {
    operator fun invoke(): Completable =
        postsRepository.deletePosts()
}