package com.rushapps.imguralbums.domain.usecase.observe

import com.rushapps.imguralbums.domain.IPostsRepository
import com.rushapps.imguralbums.domain.entity.PostEntity
import io.reactivex.Flowable
import javax.inject.Inject

class ObservePostListUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
){
    operator fun invoke(): Flowable<List<PostEntity>> =
        postsRepository.observePosts()
}