package com.rushapps.imguralbums.domain.usecase

import com.rushapps.imguralbums.domain.IPostsRepository
import com.rushapps.imguralbums.domain.entity.PostEntity
import javax.inject.Inject

class InsertCoverImageUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(post: PostEntity) =
        postsRepository.insertImage(post)

}