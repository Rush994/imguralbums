package com.rushapps.imguralbums.domain.usecase.load

import com.rushapps.imguralbums.domain.IPostsRepository
import io.reactivex.Completable
import javax.inject.Inject

class LoadPostListUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(page: Int): Completable =
        postsRepository.loadPosts(page)
}