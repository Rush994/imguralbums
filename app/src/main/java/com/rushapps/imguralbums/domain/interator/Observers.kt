package com.rushapps.imguralbums.domain.interator

import com.rushapps.imguralbums.domain.usecase.ObserveJointPostUseCase
import com.rushapps.imguralbums.domain.usecase.observe.ObserveImagesUseCase
import com.rushapps.imguralbums.domain.usecase.observe.ObservePostListUseCase
import javax.inject.Inject

class Observers @Inject constructor(
    private val observeImagesUseCase: ObserveImagesUseCase,
    private val observeJointPostUseCase: ObserveJointPostUseCase
) {
    fun images(postId: String) =
        observeImagesUseCase(postId)
    fun  jointPost(postId: String) =
        observeJointPostUseCase(postId)
}