package com.rushapps.imguralbums.domain.interator

import com.rushapps.imguralbums.domain.entity.PostEntity
import com.rushapps.imguralbums.domain.usecase.InsertCoverImageUseCase
import com.rushapps.imguralbums.domain.usecase.load.LoadCommentsUseCase
import com.rushapps.imguralbums.domain.usecase.load.LoadImagesUseCase
import javax.inject.Inject

class Loaders @Inject constructor(
    private val insertCoverImageUseCase: InsertCoverImageUseCase,
    private val loadImagesUseCase: LoadImagesUseCase,
    private val loadCommentsUseCase: LoadCommentsUseCase
) {
    fun imageInsert(post: PostEntity) =
        insertCoverImageUseCase(post)

    fun images(postId: String) =
        loadImagesUseCase(postId)

    fun comments(postId: String) =
        loadCommentsUseCase(postId)
}