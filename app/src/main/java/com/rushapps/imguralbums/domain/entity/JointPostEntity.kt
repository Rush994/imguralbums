package com.rushapps.imguralbums.domain.entity

import com.rushapps.imguralbums.presentation.viewmodel.JointPostViewModel

data class JointPostEntity(
    var post: PostEntity = PostEntity(),
    var comments: List<CommentEntity> = listOf(),
    var tags: List<TagEntity> = listOf()
)

fun JointPostEntity.toViewModel(): JointPostViewModel =
    JointPostViewModel(
        comments = comments.toViewModel(),
        tags = tags.toViewModel(),
        post = post.toViewModel()
    )