package com.rushapps.imguralbums.domain.usecase.observe

import com.rushapps.imguralbums.domain.IPostsRepository
import com.rushapps.imguralbums.domain.entity.ImageEntity
import io.reactivex.Flowable
import javax.inject.Inject

class ObserveImagesUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
){
    operator fun invoke(postId: String): Flowable<List<ImageEntity>> =
        postsRepository.observeImages(postId)
}