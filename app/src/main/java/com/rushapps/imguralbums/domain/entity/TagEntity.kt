package com.rushapps.imguralbums.domain.entity

import com.rushapps.imguralbums.presentation.viewmodel.TagViewModel

data class TagEntity(
    val name: String,
    val displayName: String,
    val followers: Int,
    val totalItems: Int
)

fun TagEntity.toViewModel(): TagViewModel =
    TagViewModel(
        name = name,
        displayName = displayName,
        followers = followers,
        totalItems = totalItems
    )

fun List<TagEntity>.toViewModel(): List<TagViewModel> =
    map { it.toViewModel() }