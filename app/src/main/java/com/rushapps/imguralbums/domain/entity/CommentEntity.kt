package com.rushapps.imguralbums.domain.entity

import com.rushapps.imguralbums.presentation.viewmodel.CommentViewModel

data class CommentEntity(
    val id: Long,
    val imageId: String,
    val comment: String,
    val author: String,
    val ups: Long,
    val downs: Long = 0,
    val platform: String
)

fun CommentEntity.toViewModel(): CommentViewModel =
    CommentViewModel(
        id = id,
        imageId = imageId,
        comment = comment,
        author = author,
        ups = ups,
        downs = downs,
        platform = platform
    )

fun List<CommentEntity>.toViewModel(): List<CommentViewModel> =
    map { it.toViewModel() }