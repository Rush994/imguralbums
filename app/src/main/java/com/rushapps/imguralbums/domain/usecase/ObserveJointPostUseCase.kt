package com.rushapps.imguralbums.domain.usecase

import com.rushapps.imguralbums.domain.IPostsRepository
import com.rushapps.imguralbums.domain.entity.JointPostEntity
import io.reactivex.Flowable
import javax.inject.Inject

class ObserveJointPostUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(postId: String): Flowable<JointPostEntity> =
        postsRepository.observeJointPost(postId)
}