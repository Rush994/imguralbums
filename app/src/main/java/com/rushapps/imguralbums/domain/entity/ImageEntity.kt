package com.rushapps.imguralbums.domain.entity

import com.rushapps.imguralbums.presentation.viewmodel.ImageViewModel

data class ImageEntity(
    val postId: String,
    val mediaId: String,
    val title: String?,
    val description: String?,
    val datetime: Long,
    val mp4: String?,
    val link: String
)

fun ImageEntity.toViewModel() =
    ImageViewModel(
        postId = postId,
        mediaId = mediaId,
        title = title,
        description = description,
        datetime = datetime,
        mp4 = mp4,
        link = link
    )

fun List<ImageEntity>.toViewModel(): List<ImageViewModel> =
    map { it.toViewModel() }