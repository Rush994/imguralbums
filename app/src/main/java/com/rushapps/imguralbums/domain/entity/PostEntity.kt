package com.rushapps.imguralbums.domain.entity

import com.rushapps.imguralbums.domain.entity.ImageEntity
import com.rushapps.imguralbums.presentation.viewmodel.PostViewModel

data class PostEntity(
    val id: String = "",
    val mediaId: String = "",
    val title: String = "",
    val datetime: Long = 0,
    val cover: String? = null,
    val owner: String = "",
    val views: Int = 0,
    val link: String = "",
    val thumbUps: Int = 0,
    val thumbDowns: Int = 0,
    val commentCount: Int = 0,
    val isAlbum: Boolean = false,
    var mediaCount: Int? = null,
    val images: List<ImageEntity> = listOf()
)

fun PostEntity.toViewModel(): PostViewModel =
    PostViewModel(
        id = id,
        mediaId = mediaId,
        title = title,
        datetime = datetime,
        cover = cover,
        owner = owner,
        views = views,
        link = link,
        thumbUps = thumbUps,
        thumbDowns = thumbDowns,
        commentCount = commentCount,
        isAlbum = isAlbum,
        mediaCount = mediaCount
    )

fun List<PostEntity>.toViewModel(): List<PostViewModel> =
    map { it.toViewModel()  }