package com.rushapps.imguralbums.domain

import com.rushapps.imguralbums.domain.entity.ImageEntity
import com.rushapps.imguralbums.domain.entity.JointPostEntity
import com.rushapps.imguralbums.domain.entity.PostEntity
import com.rushapps.imguralbums.domain.entity.TagEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface IPostsRepository {
    fun loadPost(postId: String, categoryType: Int): Completable

    fun observePosts(): Flowable<List<PostEntity>>

    fun deletePosts(): Completable

    fun observeTags(postId: String): Flowable<List<TagEntity>>

    fun loadPosts(page: Int): Completable

    fun observeImages(postId: String): Flowable<List<ImageEntity>>

    fun observeJointPost(postId: String): Flowable<JointPostEntity>

    fun insertImage(post: PostEntity): Completable

    fun loadImages(postId: String): Completable

    fun loadComments(postId: String): Completable

    fun loadTags(): Single<List<TagEntity>>
}