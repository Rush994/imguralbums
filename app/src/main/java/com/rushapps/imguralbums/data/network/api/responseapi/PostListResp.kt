package com.rushapps.imguralbums.data.network.api.responseapi

import com.google.gson.annotations.SerializedName
import com.rushapps.imguralbums.data.network.responsemodel.PostResp
import com.squareup.moshi.Json

data class PostListResp(
    @field:Json(name = "data")
    val data: List<PostResp>,
    @field:Json(name = "success")
    val success: Boolean,
    @field:Json(name = "status")
    private var status: Int
)