package com.rushapps.imguralbums.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rushapps.imguralbums.data.db.dao.*
import com.rushapps.imguralbums.data.db.model.*

@Database(
    entities = [
        PostModel::class,
        ImageModel::class,
        TagModel::class,
        CommentModel::class,
        PostTagJoinModel::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        const val TABLE_POSTS = "Posts"
        const val TABLE_IMAGES = "Images"
        const val TABLE_COMMENTS = "Comments"
        const val TABLE_POST_TAG_JOINS = "PostTagJoins"

        const val TABLE_POST_TAGS = "PostTags"
    }

    abstract fun getPostsDao(): PostsDao

    abstract fun getImagesDao(): ImagesDao

    abstract fun getTagsDao(): TagsDao

    abstract fun getCommentsDao(): CommentsDao

    abstract fun getPostTagJoinsDao(): PostTagJoinsDao
}