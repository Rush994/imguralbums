package com.rushapps.imguralbums.data.network.source

import com.rushapps.imguralbums.data.network.api.ImgurApi
import com.rushapps.imguralbums.data.network.api.responseapi.CommentListResp
import com.rushapps.imguralbums.data.network.api.responseapi.PostListResp
import com.rushapps.imguralbums.data.network.api.responseapi.PostSingleResp
import com.rushapps.imguralbums.data.network.api.responseapi.TagListDataResp
import io.reactivex.Single
import javax.inject.Inject

class PostsNetSource @Inject constructor(private val api: ImgurApi): IPostsNetSource {
    override fun getPostList(page: Int): Single<PostListResp>  =
        api.getTopPostList(page)

    override fun getPostInner(postId: String): Single<PostSingleResp> =
        api.getPostInfoInner(postId)

    override fun getCommentList(postId: String): Single<CommentListResp> =
        api.getPostCommentList(postId)

    override fun getTagList(): Single<TagListDataResp> =
        api.getTagList()
}