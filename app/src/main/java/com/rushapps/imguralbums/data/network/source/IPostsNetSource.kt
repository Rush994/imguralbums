package com.rushapps.imguralbums.data.network.source

import com.rushapps.imguralbums.data.network.api.responseapi.CommentListResp
import com.rushapps.imguralbums.data.network.api.responseapi.PostListResp
import com.rushapps.imguralbums.data.network.api.responseapi.PostSingleResp
import com.rushapps.imguralbums.data.network.api.responseapi.TagListDataResp
import io.reactivex.Single

interface IPostsNetSource {
    fun getPostList(page: Int): Single<PostListResp>

    fun getPostInner(postId: String): Single<PostSingleResp>

    fun getCommentList(postId: String): Single<CommentListResp>

    fun getTagList(): Single<TagListDataResp>
}