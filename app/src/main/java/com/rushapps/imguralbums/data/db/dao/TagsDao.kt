package com.rushapps.imguralbums.data.db.dao

import androidx.room.Dao
import com.rushapps.imguralbums.data.db.AppDatabase
import com.rushapps.imguralbums.data.db.model.TagModel

@Dao
abstract class TagsDao: BaseDao<TagModel>(AppDatabase.TABLE_POST_TAGS) {

}