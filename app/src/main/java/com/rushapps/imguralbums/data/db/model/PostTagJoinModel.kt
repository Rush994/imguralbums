package com.rushapps.imguralbums.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import com.rushapps.imguralbums.data.db.AppDatabase

@Entity(
    tableName = AppDatabase.TABLE_POST_TAG_JOINS,
    primaryKeys = (arrayOf("PostId", "TagId")),
    foreignKeys = [
        ForeignKey(
            entity = PostModel::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("PostId"),
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = TagModel::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("TagId"),
            onDelete = ForeignKey.CASCADE
        )])
data class PostTagJoinModel(
    @ColumnInfo(name = "PostId")
    var postId: String,
    @ColumnInfo(name = "TagId", index = true)
    var tagId: Long
)