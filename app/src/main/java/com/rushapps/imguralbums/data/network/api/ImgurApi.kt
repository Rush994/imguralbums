package com.rushapps.imguralbums.data.network.api

import com.rushapps.imguralbums.data.network.api.responseapi.CommentListResp
import com.rushapps.imguralbums.data.network.api.responseapi.PostListResp
import com.rushapps.imguralbums.data.network.api.responseapi.PostSingleResp
import com.rushapps.imguralbums.data.network.api.responseapi.TagListDataResp
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ImgurApi {
    companion object{
        const val BASE_URL = "https://api.imgur.com/3/"
        const val API_KEY = "046823c07ef6628"
    }

    @GET("album/{albumHash}")
    fun getAlbum(@Path("albumHash") albumId: Int)

    @GET("gallery/top/{page}?showViral=false")
    fun getTopPostList(@Path("page") page: Int): Single<PostListResp>

    @GET("gallery/album/{galleryHash}")
    fun getPostInfoInner(@Path("galleryHash") postId: String): Single<PostSingleResp>

    @GET("gallery/{imageHash}/comments/best")
    fun getPostCommentList(@Path("imageHash") postId: String): Single<CommentListResp>

    @GET("tags")
    fun getTagList(): Single<TagListDataResp>
}