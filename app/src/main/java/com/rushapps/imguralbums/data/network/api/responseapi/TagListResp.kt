package com.rushapps.imguralbums.data.network.api.responseapi

import com.google.gson.annotations.SerializedName
import com.rushapps.imguralbums.data.network.responsemodel.TagResp
import com.squareup.moshi.Json

data class TagListResp(
    @field:Json(name = "tags")
    val tags: List<TagResp>,
    @field:Json(name = "featured")
    val featured: String
)