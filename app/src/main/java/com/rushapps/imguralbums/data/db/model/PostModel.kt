package com.rushapps.imguralbums.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rushapps.imguralbums.data.db.AppDatabase
import com.rushapps.imguralbums.domain.entity.PostEntity

@Entity(tableName = AppDatabase.TABLE_POSTS)
data class PostModel(
    @PrimaryKey
    @ColumnInfo(name = "Id", index = true)
    val id: String,
    @ColumnInfo(name = "ImageId")
    var mediaId: String,
    @ColumnInfo(name = "PostTitle")
    val title: String,
    @ColumnInfo(name = "DateTime")
    val datetime: Long,
    @ColumnInfo(name = "Cover")
    val cover: String?,
    @ColumnInfo(name = "Owner")
    val owner: String,
    @ColumnInfo(name = "Views")
    val views: Int,
    @ColumnInfo(name = "Link")
    val link: String,
    @ColumnInfo(name = "ThumbUps")
    val thumbUps: Int,
    @ColumnInfo(name = "ThumbDowns")
    val thumbDowns: Int,
    @ColumnInfo(name = "CommentCount")
    val commentCount: Int,
    @ColumnInfo(name = "IsAlbum")
    val isAlbum: Boolean,
    @ColumnInfo(name = "MediaCount")
    val mediaCount: Int?
)

fun PostModel.toDomainModel(): PostEntity =
    PostEntity(
        id = id,
        mediaId = mediaId,
        title = title,
        datetime = datetime,
        cover = cover,
        owner = owner,
        views = views,
        link = link,
        thumbUps = thumbUps,
        thumbDowns = thumbDowns,
        commentCount = commentCount,
        isAlbum = isAlbum,
        mediaCount = mediaCount
    )

fun List<PostModel>.toDomainModel(): List<PostEntity> =
    map { it.toDomainModel() }