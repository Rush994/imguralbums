package com.rushapps.imguralbums.data.repository

import com.rushapps.imguralbums.data.db.model.ImageModel
import com.rushapps.imguralbums.data.db.model.toDomainModel
import com.rushapps.imguralbums.data.db.source.IPostsDBSource
import com.rushapps.imguralbums.data.network.responsemodel.toDbModel
import com.rushapps.imguralbums.data.network.responsemodel.toDomainModel
import com.rushapps.imguralbums.data.network.source.IPostsNetSource
import com.rushapps.imguralbums.domain.IPostsRepository
import com.rushapps.imguralbums.domain.entity.ImageEntity
import com.rushapps.imguralbums.domain.entity.JointPostEntity
import com.rushapps.imguralbums.domain.entity.PostEntity
import com.rushapps.imguralbums.domain.entity.TagEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class PostsRepository @Inject constructor(
    private val postNetSource: IPostsNetSource,
    private val postDBSource: IPostsDBSource
) : IPostsRepository {
    override fun loadPost(postId: String, categoryType: Int): Completable =
        postNetSource.getPostInner(postId)
            .map { it.data.toDbModel() }
            .flatMapCompletable {
                postDBSource.insertPosts(listOf(it))
            }

    override fun observePosts(): Flowable<List<PostEntity>> =
        postDBSource.observePosts()
            .map { it.toDomainModel() }

    override fun deletePosts(): Completable =
        postDBSource.deletePosts()

    override fun observeTags(postId: String): Flowable<List<TagEntity>> =
        postDBSource.observeTags(postId).map { it.toDomainModel() }

    override fun loadPosts(page: Int): Completable =
        postNetSource.getPostList(page)
            .map {
                it.data.toDbModel()
            }.flatMapCompletable {
                postDBSource.insertPosts(it)
            }

    override fun observeImages(postId: String): Flowable<List<ImageEntity>> =
        postDBSource.observeImages(postId).map { it.toDomainModel() }

    override fun observeJointPost(postId: String): Flowable<JointPostEntity> =
        postDBSource.observeComments(postId)
            .map { it.toDomainModel() }.flatMap { comments ->
                observeTags(postId).map { JointPostEntity(comments = comments, tags = it) }
            }

    override fun insertImage(post: PostEntity): Completable =
        postDBSource.insertImage(
            ImageModel(
                post.id, post.mediaId, post.title, null, post.datetime, null, post.link
            )
        )

    override fun loadImages(postId: String): Completable =
        postNetSource.getPostInner(postId)
            .map { it.data.toDbModel() }
            .flatMapCompletable {
                postDBSource.insertImages(it)
            }

    override fun loadComments(postId: String): Completable =
        postNetSource.getCommentList(postId) // "I57tpA2"
            .map { it.data.toDbModel(postId) }
            .flatMapCompletable {
                postDBSource.insertComments(it)
            }

    override fun loadTags(): Single<List<TagEntity>> =
        postNetSource.getTagList()
            .map { it.data.tags.toDomainModel() }
}