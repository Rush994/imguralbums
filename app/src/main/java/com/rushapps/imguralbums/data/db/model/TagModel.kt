package com.rushapps.imguralbums.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rushapps.imguralbums.data.db.AppDatabase
import com.rushapps.imguralbums.domain.entity.TagEntity

@Entity(tableName = AppDatabase.TABLE_POST_TAGS)
data class TagModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    val id: Long = 0,
    @ColumnInfo(name = "Name")
    val name: String,
    @ColumnInfo(name = "DisplayName")
    val displayName: String,
    @ColumnInfo(name = "Followers")
    val followers: Int,
    @ColumnInfo(name = "TotalItems")
    val totalItems: Int
)

fun TagModel.toDomainModel(): TagEntity =
    TagEntity(
        name = name,
        displayName = displayName,
        followers = followers,
        totalItems = totalItems
    )

fun List<TagModel>.toDomainModel(): List<TagEntity> =
    map { it.toDomainModel() }