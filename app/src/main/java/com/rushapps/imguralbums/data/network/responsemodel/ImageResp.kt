package com.rushapps.imguralbums.data.network.responsemodel

import com.google.gson.annotations.SerializedName
import com.rushapps.imguralbums.data.db.model.ImageModel
import com.squareup.moshi.Json

data class ImageResp(
    @field:Json(name = "id")
    val id: String,
    @field:Json(name = "title")
    val title: String?,
    @field:Json(name = "description")
    val description: String?,
    @field:Json(name = "datetime")
    val datetime: Long,
    @field:Json(name = "mp4")
    val mp4: String?,
    @field:Json(name = "link")
    val link: String
)

fun ImageResp.toDbModel(postId: String): ImageModel =
    ImageModel(
        postId = postId,
        mediaId = id,
        title = title,
        description = description,
        datetime = datetime,
        mp4 = mp4,
        link = link
    )

fun List<ImageResp>.toDbModel(postId: String): List<ImageModel> =
    map { it.toDbModel(postId) }