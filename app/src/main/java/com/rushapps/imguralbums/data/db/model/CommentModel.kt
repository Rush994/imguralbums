package com.rushapps.imguralbums.data.db.model

import androidx.room.*
import com.rushapps.imguralbums.data.db.AppDatabase
import com.rushapps.imguralbums.domain.entity.CommentEntity

@Entity(tableName = AppDatabase.TABLE_COMMENTS,
    foreignKeys = [
        ForeignKey(
            entity = PostModel::class,
            parentColumns = ["Id"],
            childColumns = ["PostId"],
            onDelete = ForeignKey.CASCADE)],
    indices = [Index("ImageId")])
data class CommentModel(
    @PrimaryKey
    @ColumnInfo(name="Id", index = true)
    val id: Long,
    @ColumnInfo(name = "ImageId")
    val imageId: String,
    @ColumnInfo(name = "Comment")
    val comment: String,
    @ColumnInfo(name = "Author")
    val author: String,
    @ColumnInfo(name = "Ups")
    val ups: Long,
    @ColumnInfo(name = "Downs")
    val downs: Long,
    @ColumnInfo(name = "Platform")
    val platform: String,

    @ColumnInfo(name = "PostId", index = true)
    val postId: String
)

fun CommentModel.toDomainModel(): CommentEntity =
    CommentEntity(
        id = id,
        imageId = imageId,
        comment = comment,
        author = author,
        ups = ups,
        downs = downs,
        platform = platform
    )

fun List<CommentModel>.toDomainModel(): List<CommentEntity> =
    map { it.toDomainModel() }