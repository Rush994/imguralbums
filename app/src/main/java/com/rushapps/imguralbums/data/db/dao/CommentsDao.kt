package com.rushapps.imguralbums.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.rushapps.imguralbums.data.db.AppDatabase
import com.rushapps.imguralbums.data.db.model.CommentModel
import io.reactivex.Flowable

@Dao
abstract class CommentsDao: BaseDao<CommentModel>(AppDatabase.TABLE_COMMENTS) {
    @Query("SELECT * FROM Comments WHERE PostId LIKE :postId")
    abstract fun observeCommentList(postId: String): Flowable<List<CommentModel>>
}