package com.rushapps.imguralbums.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.rushapps.imguralbums.data.db.AppDatabase
import com.rushapps.imguralbums.data.db.model.ImageModel
import io.reactivex.Flowable

@Dao
abstract class ImagesDao: BaseDao<ImageModel>(AppDatabase.TABLE_IMAGES) {
    @Query("SELECT * FROM Images WHERE PostId LIKE :postId")
    abstract fun observeForPost(postId: String): Flowable<List<ImageModel>>
}