package com.rushapps.imguralbums.data.network.api.responseapi

import com.squareup.moshi.Json

data class ErrorResp(
    @field:Json(name = "error")
    val error: String,
    @field:Json(name = "request")
    val request: String,
    @field:Json(name = "method")
    val method: String
)