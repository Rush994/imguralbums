package com.rushapps.imguralbums.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.rushapps.imguralbums.data.db.AppDatabase
import com.rushapps.imguralbums.data.db.model.PostTagJoinModel
import com.rushapps.imguralbums.data.db.model.TagModel
import io.reactivex.Flowable

@Dao
abstract class PostTagJoinsDao: BaseDao<PostTagJoinModel>(AppDatabase.TABLE_POST_TAG_JOINS){
    @Query("""SELECT * FROM PostTags INNER JOIN PostTagJoins ON
                    PostTags.Id=PostTagJoins.tagId WHERE
                    PostTagJoins.postId LIKE :postId""")
    abstract fun observePostTags(postId: String): Flowable<List<TagModel>>
}