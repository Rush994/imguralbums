package com.rushapps.imguralbums.data.network.api.responseapi

import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json

data class TagListDataResp(
    @field:Json(name = "data")
    val data: TagListResp,
    @field:Json(name = "success")
    val success: Boolean,
    @field:Json(name = "status")
    private var status: Int
)