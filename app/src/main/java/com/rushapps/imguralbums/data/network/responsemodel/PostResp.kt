package com.rushapps.imguralbums.data.network.responsemodel

import com.rushapps.imguralbums.data.db.model.ImageModel
import com.rushapps.imguralbums.data.db.model.JointPostModel
import com.rushapps.imguralbums.data.db.model.PostModel
import com.squareup.moshi.Json

data class PostResp(
    @field:Json(name = "id")
    val id: String,
    @field:Json(name = "title")
    val title: String,
    @field:Json(name = "datetime")
    val datetime: Long,
    @field:Json(name = "cover")
    val cover: String,
    @field:Json(name = "account_url")
    val owner: String,
    @field:Json(name = "views")
    val views: Int,
    @field:Json(name = "link")
    val link: String,
    @field:Json(name = "ups")
    val thumbUps: Int,
    @field:Json(name = "downs")
    val thumbDowns: Int,
    @field:Json(name = "comment_count")
    val commentCount: Int,
    @field:Json(name = "is_album")
    val isAlbum: Boolean,
    @field:Json(name = "images_count")
    val mediaCount: Int?,
    @field:Json(name = "tags")
    val tags: List<TagResp>,
    @field:Json(name = "images")
    val images: List<ImageResp>?
)

fun PostResp.toDbModel(): JointPostModel {
    val post = PostModel(
        id = id, mediaId = id, title = title, datetime = datetime,
        cover = cover, owner = owner, views = views, link = link, thumbDowns = thumbDowns,
        thumbUps = thumbUps, commentCount = commentCount, isAlbum = isAlbum, mediaCount = mediaCount
    )
    var images: List<ImageModel> = listOf()
    this.images?.let {
        images = it.toDbModel(id)
    }
    return JointPostModel(
        post = post,
        images = images,
        tags = tags.toDbModel()
    )
}

fun List<PostResp>.toDbModel():List<JointPostModel> =
    map { it.toDbModel() }