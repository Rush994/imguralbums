package com.rushapps.imguralbums.data.db.model

data class JointPostModel(
    val post: PostModel,
    val images: List<ImageModel> = listOf(),
    val tags: List<TagModel>
)