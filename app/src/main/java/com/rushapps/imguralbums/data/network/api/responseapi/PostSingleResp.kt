package com.rushapps.imguralbums.data.network.api.responseapi

import com.google.gson.annotations.SerializedName
import com.rushapps.imguralbums.data.network.responsemodel.PostResp
import com.squareup.moshi.Json

data class PostSingleResp(
    @field:Json(name = "data")
    val data: PostResp,
    @field:Json(name = "success")
    val success: Boolean,
    @field:Json(name = "status")
    val status: Int
)