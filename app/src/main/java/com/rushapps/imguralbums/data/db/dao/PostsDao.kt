package com.rushapps.imguralbums.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.rushapps.imguralbums.data.db.AppDatabase
import com.rushapps.imguralbums.data.db.model.PostModel
import io.reactivex.Flowable

@Dao
abstract class PostsDao: BaseDao<PostModel>(AppDatabase.TABLE_POSTS) {
    @Query("SELECT * FROM Posts")
    abstract fun observePostList(): Flowable<List<PostModel>>

    @Query("SELECT * FROM Posts WHERE Id LIKE :postId")
    abstract fun findPost(postId: String): Flowable<PostModel>

    @Query("DELETE FROM Posts")
    abstract fun deletePosts()
}