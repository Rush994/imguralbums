package com.rushapps.imguralbums.data.network.responsemodel

import com.google.gson.annotations.SerializedName
import com.rushapps.imguralbums.data.db.model.TagModel
import com.rushapps.imguralbums.domain.entity.TagEntity
import com.squareup.moshi.Json

data class TagResp(
    @field:Json(name = "name")
    val name: String,
    @field:Json(name = "display_name")
    val displayName: String,
    @field:Json(name = "followers")
    val followers: Int,
    @field:Json(name = "total_items")
    val totalItems: Int
)

fun TagResp.toDbModel() : TagModel =
    TagModel(
        name = name,
        displayName = displayName,
        followers = followers,
        totalItems = totalItems
    )

fun List<TagResp>.toDbModel(): List<TagModel> =
    map { it.toDbModel() }

fun TagResp.toDomainModel(): TagEntity =
    TagEntity(
        name = name,
        displayName = displayName,
        followers = followers,
        totalItems = totalItems
    )

fun List<TagResp>.toDomainModel(): List<TagEntity> =
    map { it.toDomainModel() }