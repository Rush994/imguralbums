package com.rushapps.imguralbums.data.network.responsemodel

import com.google.gson.annotations.SerializedName
import com.rushapps.imguralbums.data.db.model.CommentModel
import com.squareup.moshi.Json

data class CommentResp(
    @field:Json(name = "id")
    val id: Long,
    @field:Json(name = "image_id")
    val imageId: String,
    @field:Json(name = "comment")
    val comment: String,
    @field:Json(name = "author")
    val author: String,
    @field:Json(name = "ups")
    val ups: Long,
    @field:Json(name = "downs")
    val downs: Long,
    @field:Json(name = "platform")
    val platform: String
)

fun CommentResp.toDbModel(postId: String): CommentModel =
    CommentModel(
        id = id,
        imageId = imageId,
        comment = comment,
        author = author,
        ups = ups,
        downs = downs,
        platform = platform,
        postId = postId
    )

fun List<CommentResp>.toDbModel(postId: String): List<CommentModel> =
    map { it.toDbModel(postId) }

