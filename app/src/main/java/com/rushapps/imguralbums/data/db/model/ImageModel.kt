package com.rushapps.imguralbums.data.db.model

import androidx.room.*
import com.rushapps.imguralbums.data.db.AppDatabase
import com.rushapps.imguralbums.domain.entity.ImageEntity

@Entity(tableName = AppDatabase.TABLE_IMAGES,
    foreignKeys = [
        ForeignKey(
            entity = PostModel::class,
            parentColumns = ["Id"],
            childColumns = ["PostId"],
            onDelete = ForeignKey.CASCADE)],
    indices = [Index("MediaId")]
)
data class ImageModel(
    @ColumnInfo(name = "PostId", index = true)
    val postId: String,
    @PrimaryKey
    @ColumnInfo(name = "MediaId")
    val mediaId: String,
    @ColumnInfo(name = "Title")
    val title: String?,
    @ColumnInfo(name ="Description")
    val description: String?,
    @ColumnInfo(name = "DateTime")
    val datetime: Long,
    @ColumnInfo(name = "Mp4")
    val mp4: String?,
    @ColumnInfo(name = "Link")
    val link: String
)

fun ImageModel.toDomainModel(): ImageEntity =
    ImageEntity(
        postId = postId,
        mediaId = mediaId,
        title = title,
        description = description,
        datetime = datetime,
        mp4 = mp4,
        link = link
    )

fun List<ImageModel>.toDomainModel(): List<ImageEntity> =
    map { it.toDomainModel() }